FROM node:16-alpine

ENV NODE_ENV=production

WORKDIR /usr/src/app

COPY package.json ./
COPY yarn.lock ./

RUN yarn --prod

COPY . .

EXPOSE 80

CMD ["yarn", "start"]
