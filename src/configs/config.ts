import convict from 'convict';

const config = convict({
  env: {
    doc: 'The application environment.',
    format: ['production', 'development', 'test'],
    default: 'development',
    env: 'NODE_ENV',
  },
  api: {
    host: {
      doc: 'API host name',
      format: String,
      default: 'localhost',
      env: 'API_HOST',
    },
    port: {
      doc: 'The api port to bind.',
      format: 'port',
      default: 8080,
      env: 'API_PORT',
    },
  },
});

// Perform validation
config.validate({ allowed: 'strict' });

export default config;
