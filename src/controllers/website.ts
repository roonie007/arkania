import scraper from '../lib/scraper';

export const archive = async (url: string) => {
  const { timestamp } = await scraper(url);

  return timestamp;
};
