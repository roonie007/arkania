import { fromRoman, toRoman } from './roman';

describe('Roman - From roman to number', () => {
  it('Should return 39', () => {
    expect(fromRoman('XXXIX')).toEqual(39);
  });
  it('Should return 246', () => {
    expect(fromRoman('CCXLVI')).toEqual(246);
  });
  it('Should return 789', () => {
    expect(fromRoman('DCCLXXXIX')).toEqual(789);
  });
  it('Should return 2421', () => {
    expect(fromRoman('MMCDXXI')).toEqual(2421);
  });

  it('Should return 160', () => {
    expect(fromRoman('CLX')).toEqual(160);
  });

  it('Should return 207', () => {
    expect(fromRoman('CCVII')).toEqual(207);
  });
  it('Should return 1009', () => {
    expect(fromRoman('MIX')).toEqual(1009);
  });
  it('Should return 1066', () => {
    expect(fromRoman('MLXVI')).toEqual(1066);
  });
  it('Should return 1776', () => {
    expect(fromRoman('MDCCLXXVI')).toEqual(1776);
  });
  it('Should return 1918', () => {
    expect(fromRoman('MCMXVIII')).toEqual(1918);
  });
  it('Should return 1954', () => {
    expect(fromRoman('MCMLIV')).toEqual(1954);
  });
  it('Should return 2014', () => {
    expect(fromRoman('MMXIV')).toEqual(2014);
  });
  it('Should return 3999', () => {
    expect(fromRoman('MMMCMXCIX')).toEqual(3999);
  });
});

describe('Roman - From number to roman', () => {
  it('Should return XXXIX', () => {
    expect(toRoman(39)).toEqual('XXXIX');
  });
  it('Should return CCXLVI', () => {
    expect(toRoman(246)).toEqual('CCXLVI');
  });
  it('Should return DCCLXXXIX', () => {
    expect(toRoman(789)).toEqual('DCCLXXXIX');
  });
  it('Should return MMCDXXI', () => {
    expect(toRoman(2421)).toEqual('MMCDXXI');
  });

  it('Should return CLX', () => {
    expect(toRoman(160)).toEqual('CLX');
  });

  it('Should return CCVII', () => {
    expect(toRoman(207)).toEqual('CCVII');
  });
  it('Should return MIX', () => {
    expect(toRoman(1009)).toEqual('MIX');
  });
  it('Should return MLXVI', () => {
    expect(toRoman(1066)).toEqual('MLXVI');
  });
  it('Should return MDCCLXXVI', () => {
    expect(toRoman(1776)).toEqual('MDCCLXXVI');
  });
  it('Should return MCMXVIII', () => {
    expect(toRoman(1918)).toEqual('MCMXVIII');
  });
  it('Should return MCMLIV', () => {
    expect(toRoman(1954)).toEqual('MCMLIV');
  });
  it('Should return MMXIV', () => {
    expect(toRoman(2014)).toEqual('MMXIV');
  });
  it('Should return MMMCMXCIX', () => {
    expect(toRoman(3999)).toEqual('MMMCMXCIX');
  });
  it('Should throw an error if number is greater than 3999', () => {
    expect(() => toRoman(4000)).toThrowError();
  });
});
