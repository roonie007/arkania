import fs from 'fs-extra';
import path from 'path';
import { scrapingDirectory } from '../lib/scraper';
import { archive } from './website';

describe('Website - Archive', () => {
  it('Should archive a website', async () => {
    const timestamp = await archive('https://google.fr');

    const scrapingFolderExist = await fs.pathExists(
      path.join(scrapingDirectory, timestamp.toString(), 'https://google.fr'),
    );
    expect(scrapingFolderExist).toBeTruthy();
  });
});
