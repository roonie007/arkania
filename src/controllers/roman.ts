import { numberToRoman, RomanNumbersDecimalPlaces, romanToNumber } from '../lib/romanNumbers';

export const fromRoman = (numeral: string) => {
  const units = romanToNumber(numeral.toLowerCase().trim(), 'units');
  const tens = romanToNumber(units.romanNumber, 'tens');
  const hundreds = romanToNumber(tens.romanNumber, 'hundreds');

  return hundreds.romanNumber.length * 1000 + hundreds.number * 100 + tens.number * 10 + units.number;
};

export const toRoman = (number: number) => {
  if (number > 3999) {
    throw new Error('Max supported number is 3999');
  }

  return number
    .toString()
    .padStart(4, '0')
    .split('')
    .map((strNumber, index) => {
      let type: RomanNumbersDecimalPlaces = 'units';
      switch (index) {
        case 0:
          type = 'thousands';
          break;
        case 1:
          type = 'hundreds';
          break;
        case 2:
          type = 'tens';
          break;
        case 3:
          type = 'units';
          break;
      }

      return numberToRoman(parseInt(strNumber), type);
    })
    .join('')
    .toUpperCase();
};
