// Require the framework and instantiate it
import fastify from 'fastify';
import fastifyStatic from 'fastify-static';
import path from 'path';

import config from './configs/config';
import { scrapingDirectory } from './lib/scraper';

import loadRoutes from './routes';

const app = fastify({ logger: true, disableRequestLogging: true, ignoreTrailingSlash: true });

// Declare a route
app.get('/', async () => {
  return { welcome: 'arkania' };
});

loadRoutes(app);

app.register(fastifyStatic, {
  root: path.resolve(scrapingDirectory),
  prefix: '/view/',
  redirect: true,
  index: ['index.html'],
});

// Run the server!
export default async () => {
  try {
    await app.listen(config.get('api.port'), '0.0.0.0');
  } catch (err) {
    app.log.error(err);
    process.exit(1);
  }
};
