import { FastifyInstance } from 'fastify';
import S from 'fluent-json-schema';

import { fromRoman, toRoman } from '../controllers/roman';
import { archive } from '../controllers/website';

const fromRomanSchemaValidation = {
  params: S.object().prop('numeral', S.string().minLength(1).required()),
};

const toRomanSchemaValidation = {
  params: S.object().prop('number', S.number().minimum(1).maximum(3999).required()),
};

const archiveSchemaValidation = {
  params: S.object().prop('url', S.string().required()),
};

export default (fastify: FastifyInstance) => {
  fastify.get<{ Params: { numeral: string } }>(
    '/fromRoman/:numeral',
    { schema: fromRomanSchemaValidation },
    async ({ params: { numeral } }) => fromRoman(numeral),
  );

  fastify.get<{ Params: { number: number } }>(
    '/toRoman/:number',
    { schema: toRomanSchemaValidation },
    async ({ params: { number } }) => toRoman(number),
  );

  fastify.get<{ Params: { url: string } }>(
    '/archive/http://:url',
    { schema: archiveSchemaValidation },
    async ({ params: { url } }) => {
      return archive('http://' + url);
    },
  );
  fastify.get<{ Params: { url: string } }>(
    '/archive/https://:url',
    { schema: archiveSchemaValidation },
    async ({ params: { url } }) => {
      return archive('https://' + url);
    },
  );
};
