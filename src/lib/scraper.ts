import scrape from 'website-scraper';
import path from 'path';
import { URL } from 'url';

export const scrapingDirectory = './scraping';

export const getProtocolAndHostnameFromUrl = (url: string) => {
  const { protocol, hostname } = new URL(url);

  return { protocol, hostname };
};

export default async (url: string) => {
  const timestamp = Date.now();
  const { protocol, hostname } = getProtocolAndHostnameFromUrl(url);

  const result = await scrape({
    urls: [`${protocol}//${hostname}`],
    directory: path.join(scrapingDirectory, timestamp.toString(), url),
    maxRecursiveDepth: 4,
  });

  return {
    timestamp,
    result,
  };
};
