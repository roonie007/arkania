export const romanNumbersDefinition = {
  units: {
    start: 'i',
    middle: 'v',
    end: 'x',
    regex: /ix|viii|vii|vi|v|iv|iii|ii|i/im,
  },
  tens: {
    start: 'x',
    middle: 'l',
    end: 'c',
    regex: /xc|lxxx|lxx|lx|l|xl|xxx|xx|x/im,
  },
  hundreds: {
    start: 'c',
    middle: 'd',
    end: 'm',
    regex: /cm|dccc|dcc|dc|d|cd|ccc|cc|c/im,
  },
  thousands: {
    start: 'm',
    regex: /mmm|mm|m/im,
    middle: '',
    end: '',
  },
};

export type RomanNumbersDecimalPlaces = keyof typeof romanNumbersDefinition;
export type RomanNumberDefinition = typeof romanNumbersDefinition;

/**
 *
 * @param symbol
 * @param type
 */
export const romanToNumber = (romanNumber: string, type: RomanNumbersDecimalPlaces) => {
  const { start, middle, end, regex } = romanNumbersDefinition[type];
  const romanNumberMatches = romanNumber.match(regex);

  if (!romanNumberMatches) {
    return { number: 0, romanNumber };
  }

  const matchedRomanNumber = (romanNumberMatches[0] || '').trim();

  let number = 0;
  if (matchedRomanNumber.startsWith(start) && matchedRomanNumber.endsWith(middle)) {
    number = 4;
  } else if (matchedRomanNumber.startsWith(start) && matchedRomanNumber.endsWith(end)) {
    number = 9;
  } else if (matchedRomanNumber === middle) {
    number = 5;
  } else if (matchedRomanNumber.startsWith(start) && matchedRomanNumber.endsWith(start)) {
    number = matchedRomanNumber.length;
  } else {
    number = matchedRomanNumber.length + 4;
  }

  return { number, romanNumber: romanNumber.replace(matchedRomanNumber, '') };
};

export const numberToRoman = (number: number, type: RomanNumbersDecimalPlaces) => {
  const { start, middle, end } = romanNumbersDefinition[type];

  if (number < 4) {
    return start.repeat(number);
  } else if (number === 4) {
    return `${start}${middle}`;
  } else if (number === 5) {
    return middle;
  } else if (number === 9) {
    return `${start}${end}`;
  } else {
    return `${middle}${start.repeat(number - 5)}`;
  }
};
