import { getProtocolAndHostnameFromUrl } from './scraper';

describe('Lib - getProtocolAndHostnameFromUrl', () => {
  it('Should return http + google.fr', () => {
    expect(getProtocolAndHostnameFromUrl('http://google.fr')).toEqual(
      expect.objectContaining({ protocol: 'http:', hostname: 'google.fr' }),
    );
  });

  it('Should return https + en.wikipedia.org', () => {
    expect(getProtocolAndHostnameFromUrl('https://en.wikipedia.org/wiki/Roman_numerals#Large_numbers')).toEqual(
      expect.objectContaining({ protocol: 'https:', hostname: 'en.wikipedia.org' }),
    );
  });

  it('Should throw an error when url is invalid', () => {
    expect(() => getProtocolAndHostnameFromUrl('soem thing invalid')).toThrowError('Invalid URL');
  });
});
