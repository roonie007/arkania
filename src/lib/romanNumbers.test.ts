import { romanToNumber, numberToRoman } from './romanNumbers';

describe('Lib - romanToNumber', () => {
  it('Should return number 9 and romanNumber mmmcmxc', () => {
    expect(romanToNumber('mmmcmxcix', 'units')).toEqual(expect.objectContaining({ number: 9, romanNumber: 'mmmcmxc' }));
  });

  it('Should return number 9 and romanNumber mmmcm', () => {
    expect(romanToNumber('mmmcmxc', 'tens')).toEqual(expect.objectContaining({ number: 9, romanNumber: 'mmmcm' }));
  });

  it('Should return number 9 and romanNumber mmm', () => {
    expect(romanToNumber('mmmcm', 'hundreds')).toEqual(expect.objectContaining({ number: 9, romanNumber: 'mmm' }));
  });

  it('Should return number 1', () => {
    expect(romanToNumber('i', 'units')).toEqual(expect.objectContaining({ number: 1, romanNumber: '' }));
    expect(romanToNumber('x', 'tens')).toEqual(expect.objectContaining({ number: 1, romanNumber: '' }));
    expect(romanToNumber('c', 'hundreds')).toEqual(expect.objectContaining({ number: 1, romanNumber: '' }));
  });

  it('Should return number 2', () => {
    expect(romanToNumber('ii', 'units')).toEqual(expect.objectContaining({ number: 2, romanNumber: '' }));
    expect(romanToNumber('xx', 'tens')).toEqual(expect.objectContaining({ number: 2, romanNumber: '' }));
    expect(romanToNumber('cc', 'hundreds')).toEqual(expect.objectContaining({ number: 2, romanNumber: '' }));
  });

  it('Should return number 3', () => {
    expect(romanToNumber('iii', 'units')).toEqual(expect.objectContaining({ number: 3, romanNumber: '' }));
    expect(romanToNumber('xxx', 'tens')).toEqual(expect.objectContaining({ number: 3, romanNumber: '' }));
    expect(romanToNumber('ccc', 'hundreds')).toEqual(expect.objectContaining({ number: 3, romanNumber: '' }));
  });

  it('Should return number 4', () => {
    expect(romanToNumber('iv', 'units')).toEqual(expect.objectContaining({ number: 4, romanNumber: '' }));
    expect(romanToNumber('xl', 'tens')).toEqual(expect.objectContaining({ number: 4, romanNumber: '' }));
    expect(romanToNumber('cd', 'hundreds')).toEqual(expect.objectContaining({ number: 4, romanNumber: '' }));
  });

  it('Should return number 5', () => {
    expect(romanToNumber('v', 'units')).toEqual(expect.objectContaining({ number: 5, romanNumber: '' }));
    expect(romanToNumber('l', 'tens')).toEqual(expect.objectContaining({ number: 5, romanNumber: '' }));
    expect(romanToNumber('d', 'hundreds')).toEqual(expect.objectContaining({ number: 5, romanNumber: '' }));
  });

  it('Should return number 6', () => {
    expect(romanToNumber('vi', 'units')).toEqual(expect.objectContaining({ number: 6, romanNumber: '' }));
    expect(romanToNumber('lx', 'tens')).toEqual(expect.objectContaining({ number: 6, romanNumber: '' }));
    expect(romanToNumber('dc', 'hundreds')).toEqual(expect.objectContaining({ number: 6, romanNumber: '' }));
  });
  it('Should return number 7', () => {
    expect(romanToNumber('vii', 'units')).toEqual(expect.objectContaining({ number: 7, romanNumber: '' }));
    expect(romanToNumber('lxx', 'tens')).toEqual(expect.objectContaining({ number: 7, romanNumber: '' }));
    expect(romanToNumber('dcc', 'hundreds')).toEqual(expect.objectContaining({ number: 7, romanNumber: '' }));
  });
  it('Should return number 8', () => {
    expect(romanToNumber('viii', 'units')).toEqual(expect.objectContaining({ number: 8, romanNumber: '' }));
    expect(romanToNumber('lxxx', 'tens')).toEqual(expect.objectContaining({ number: 8, romanNumber: '' }));
    expect(romanToNumber('dccc', 'hundreds')).toEqual(expect.objectContaining({ number: 8, romanNumber: '' }));
  });
  it('Should return number 9', () => {
    expect(romanToNumber('ix', 'units')).toEqual(expect.objectContaining({ number: 9, romanNumber: '' }));
    expect(romanToNumber('xc', 'tens')).toEqual(expect.objectContaining({ number: 9, romanNumber: '' }));
    expect(romanToNumber('cm', 'hundreds')).toEqual(expect.objectContaining({ number: 9, romanNumber: '' }));
  });
});

describe('Lib - numberToRoman', () => {
  it('Should return i', () => {
    expect(numberToRoman(1, 'units')).toEqual('i');
  });
  it('Should return x', () => {
    expect(numberToRoman(1, 'tens')).toEqual('x');
  });
  it('Should return c', () => {
    expect(numberToRoman(1, 'hundreds')).toEqual('c');
  });
  it('Should return m', () => {
    expect(numberToRoman(1, 'thousands')).toEqual('m');
  });

  it('Should return ii', () => {
    expect(numberToRoman(2, 'units')).toEqual('ii');
  });
  it('Should return xx', () => {
    expect(numberToRoman(2, 'tens')).toEqual('xx');
  });
  it('Should return cc', () => {
    expect(numberToRoman(2, 'hundreds')).toEqual('cc');
  });
  it('Should return mm', () => {
    expect(numberToRoman(2, 'thousands')).toEqual('mm');
  });

  it('Should return iii', () => {
    expect(numberToRoman(3, 'units')).toEqual('iii');
  });
  it('Should return xxx', () => {
    expect(numberToRoman(3, 'tens')).toEqual('xxx');
  });
  it('Should return ccc', () => {
    expect(numberToRoman(3, 'hundreds')).toEqual('ccc');
  });
  it('Should return mmm', () => {
    expect(numberToRoman(3, 'thousands')).toEqual('mmm');
  });

  it('Should return iv', () => {
    expect(numberToRoman(4, 'units')).toEqual('iv');
  });
  it('Should return xl', () => {
    expect(numberToRoman(4, 'tens')).toEqual('xl');
  });
  it('Should return cd', () => {
    expect(numberToRoman(4, 'hundreds')).toEqual('cd');
  });

  it('Should return v', () => {
    expect(numberToRoman(5, 'units')).toEqual('v');
  });
  it('Should return l', () => {
    expect(numberToRoman(5, 'tens')).toEqual('l');
  });
  it('Should return d', () => {
    expect(numberToRoman(5, 'hundreds')).toEqual('d');
  });

  it('Should return vi', () => {
    expect(numberToRoman(6, 'units')).toEqual('vi');
  });
  it('Should return lx', () => {
    expect(numberToRoman(6, 'tens')).toEqual('lx');
  });
  it('Should return dc', () => {
    expect(numberToRoman(6, 'hundreds')).toEqual('dc');
  });

  it('Should return vii', () => {
    expect(numberToRoman(7, 'units')).toEqual('vii');
  });
  it('Should return lxx', () => {
    expect(numberToRoman(7, 'tens')).toEqual('lxx');
  });
  it('Should return dcc', () => {
    expect(numberToRoman(7, 'hundreds')).toEqual('dcc');
  });

  it('Should return viii', () => {
    expect(numberToRoman(8, 'units')).toEqual('viii');
  });
  it('Should return lxxx', () => {
    expect(numberToRoman(8, 'tens')).toEqual('lxxx');
  });
  it('Should return dccc', () => {
    expect(numberToRoman(8, 'hundreds')).toEqual('dccc');
  });

  it('Should return ix', () => {
    expect(numberToRoman(9, 'units')).toEqual('ix');
  });
  it('Should return xc', () => {
    expect(numberToRoman(9, 'tens')).toEqual('xc');
  });
  it('Should return cm', () => {
    expect(numberToRoman(9, 'hundreds')).toEqual('cm');
  });
});
